### A Pluto.jl notebook ###
# v0.19.38

using Markdown
using InteractiveUtils

# ╔═╡ efd0d5fd-182a-4f71-958c-d8342374eaeb
using PlutoUI

# ╔═╡ a8f52830-4534-4043-ada1-4e38f1795d10
using StatsPlots, RDatasets,Statistics,Distributions,StatsBase,ANOVA

# ╔═╡ 5bc74a25-0d17-4ede-8ddf-18670f00ce31
using MLJ,MLJLinearModels,CategoricalArrays, DataFrames

# ╔═╡ 400f3167-06d0-4379-875a-319e5462cd0a
using MLJLIBSVMInterface

# ╔═╡ 302a0c2a-b98a-4887-a451-3af6bde6c3e5
md"""
# Assignment 1

|                  |                                       |
|-----------------------|--------------------------------------------|
| Course Title          | Trends in Artificial Intelligence and Machine Learning |
| Course Code           | TAI911S                                    |
| Assessment            | First Assignment                           |
| Released on           | 25/03/2024                                 |
| Due Date              | 09/04/2024                                 |

| Student Name | Student Number |
|--------------|----------------|
| Hubert Mouton       | 219079714  |
| Erikson Victor      | 224096508  |
| Paulina Haixula     | 200931296  |
| Ndeiweda Ndaamekele | 219055777  | 


# Problem

## Problem Description

The table below contains URLs of economic datasets (__Ecdat__) compiled in the **RDatasets** package. Given the group number assigned to you, select the corresponding dataset and perform the following tasks:

- complete an exploratory data **analysis**;
- select the most  appropriate **regression function**. You will motivate your selection;
- build a model based on the selected regression function and test it.

All implementations should be completed in the **Julia** Programming language.

"""

# ╔═╡ bd0c048e-a055-49c7-b3f1-addf30bd90dc
md"""
### Import Dependencies
"""

# ╔═╡ 60a6278d-3108-4b2d-b537-4e72a4f9ff61
import LIBSVM,DecisionTree,MLJScikitLearnInterface,MLJFlux

# ╔═╡ 7f2b9cae-248c-472b-9e4c-40ba980c7af9
md"""
### Import data from RDatasets
"""

# ╔═╡ 4ccc6a2b-8393-41a1-8cc7-53fd7c962cd0
earn_data = dataset("Ecdat","Earnings")

# ╔═╡ 767c3690-f08a-49ba-b41a-dd3f2fe5fb15
md"""
# Exploratory Data Analysis.
"""

# ╔═╡ 124f1b71-3d1f-4ad8-afde-28829bd9f75d
 rename!(earn_data, :Y => :Earnings, :Age => :Age_Group)

# ╔═╡ b4438d2c-0525-40e2-b6f2-dc88bf09853e
select!(earn_data, [:Earnings, :Age_Group])

# ╔═╡ 7250a2b6-c5a7-459f-aba7-6e914d66200c
schema(earn_data)

# ╔═╡ 8b94ea34-0bba-4744-8cd3-fa71c78102ce
describe(earn_data)

# ╔═╡ fe1e1728-8b9f-439c-969a-320a1d79a0b6
countmap(earn_data[!, :Earnings])

# ╔═╡ f7a2214b-98c3-4bd3-b0a7-6c0de2e62fbb
begin
	mu = mean(earn_data.Earnings)
	sigma = std(earn_data.Earnings)
	xD = range(minimum(earn_data.Earnings), stop=maximum(earn_data.Earnings), length=length(earn_data.Earnings))
	yD = pdf.(Normal(mu, sigma), xD)

end

# ╔═╡ 00298d9a-87cf-4369-a27c-c9afa743f3e9
boxplot(earn_data.Age_Group, earn_data.Earnings, xlabel="Age Groups",group=earn_data.Age_Group, ylabel="Earnings", title="Earnings by Age Groups", legend=:bottomright)


# ╔═╡ ac2f41c0-04a1-42a2-8f73-020260a1cf0c
begin
	histogram(1:length(earn_data.Earnings),earn_data.Earnings, xlabel="Counts", ylabel="Earnings", title="Histogram of Earnings")
	plot!(xD, yD * length(earn_data.Earnings) * (maximum(earn_data.Earnings) - minimum(earn_data.Earnings)) / 10, color=:red, label="Distribution")
end

# ╔═╡ f8db4414-b048-4851-9eb2-26b42d51f58c
scatter(earn_data.Earnings, group=earn_data.Age_Group,xlabel="Counts", ylabel="Earnings", title="Earnings Distribution by Age Group",markersize=6, markerstrokewidth=0, marker=:circle, legend=:topleft,size=(800, 400))


# ╔═╡ aa5e607c-b529-4362-ad59-ae561e683fd2
begin
	groups = groupby(earn_data, :Age_Group)
	group1_df = groups[1]
	group2_df = groups[2]
	group3_df = groups[3]
end

# ╔═╡ d386f6b5-1b51-4d66-8078-cbf7d6ce1706
begin
	u = group1_df.Age_Group
	v = group2_df.Age_Group
	w = group3_df.Age_Group
end

# ╔═╡ a3ed4fab-568e-4999-a340-c6549b8253d2
scatter(earn_data.Earnings,1:length(earn_data.Earnings))

# ╔═╡ f4a6022f-01d7-4925-ac08-6998aa8647a4
countmap(earn_data.Age_Group)

# ╔═╡ 3a770051-bb92-4260-93ac-98ea2b6a7082
begin
	correlation = cor(1:length(earn_data.Age_Group), earn_data.Earnings)
	println("Correlation coefficient: ", correlation)
end

# ╔═╡ 716ed545-eb52-4d22-b4aa-0ba573161c16
md"""
### Removal of Outliers
"""

# ╔═╡ cde0eb5a-05c3-43a6-b05d-65fcb7de9225
begin
	q1 = quantile(earn_data.Earnings, 0.25)
	q3 = quantile(earn_data.Earnings, 0.75)
	iqr = q3 - q1
	lower_bound = q1 - 1.5 * iqr
	upper_bound = q3 + 1.5 * iqr
end

# ╔═╡ d166545b-5723-4f2e-84a8-ba481109ffa6
outliers = (earn_data.Earnings .< lower_bound) .| (earn_data.Earnings .> upper_bound)

# ╔═╡ 52270e95-0922-4f1c-b450-b13484e49ef5
clean_data = earn_data[.!outliers, :]

# ╔═╡ 82601ef7-855c-403e-b206-7fb01ecfadab
boxplot(clean_data.Age_Group, clean_data.Earnings, group=clean_data.Age_Group)

# ╔═╡ e33fd088-6dc9-4f86-b384-7d5f0b86a974
md"""
### Model Implementation
- [x] SVM-based Classifier with Sigmoid Kernel
- [ ] RandomForest-based Classifier
- [ ] KNN-based Classifier
"""

# ╔═╡ f8e38125-7708-4908-a0a1-c609594569d1
coerce!(clean_data, :Earnings => MLJ.Continuous, :Age_Group => Multiclass)

# ╔═╡ e6608a84-e856-42a1-8f78-97b9e6aeb78e
md"""
#### Extract the features and targets
"""

# ╔═╡ 7e317040-dfdf-4d5a-8e9f-bd80423eca4c
X = clean_data[:, ["Earnings"]]

# ╔═╡ ebd5380c-ab2d-469d-93d8-5779dbe0f21c
y = clean_data[:, "Age_Group"]

# ╔═╡ 964aa138-0fa5-4289-80ee-b9aa9c0657cd
md"""
#### Models that are compatable with our dataset
"""

# ╔═╡ bed92cf6-f22e-4997-bbc1-8063b369c681
begin
	compat_df = DataFrame(models(matching(X,y)))
	print(compat_df.name)
end

# ╔═╡ 3e5af368-9cf6-4270-86e1-8e77b8961f4c
md"""
#### Split into train and test set using 80% for training and 20% for testing
"""

# ╔═╡ 504a1778-a24d-4272-bc72-dbb9a3fff103
train_set, test_set = partition(eachindex(y), shuffle=true,0.70)

# ╔═╡ 536a252d-7397-496c-a457-b1da5c531c30
md"""
### SVM Based Classifier
"""

# ╔═╡ cd1c5f07-4488-4efd-bcd6-06f70150b74b
ProbabilisticNuSVC = @load ExtraTreesClassifier pkg=MLJScikitLearnInterface

# ╔═╡ b407062c-caa3-42b8-a53e-91e331bec8af
model = ProbabilisticNuSVC()

# ╔═╡ cddd970f-ffe9-40c3-9129-fbfbf3f4974d
mach = machine(model, X, y,scitype_check_level=0)

# ╔═╡ 791c9a2d-f18e-4028-97dc-bc1339898793
the_predictor = fit!(mach,rows=train_set,verbosity=0)

# ╔═╡ b2e899dd-66c3-4d28-bd02-5bb2b238fa20
MLJ.predict(the_predictor)

# ╔═╡ f28336f7-548e-4760-8af8-49fa34a0d752
fitted_params(the_predictor)

# ╔═╡ da0c597c-5efe-463f-8e17-1a6a4032f7a3
accuracy(predict_mode(the_predictor, rows=test_set), y[test_set])

# ╔═╡ ae9fb2fe-37ee-47ef-923f-bec72e51e930
accuracy(predict_mode(the_predictor, rows=train_set), y[train_set])

# ╔═╡ a5dd4a8d-849a-458d-bff4-c7aa3f2b5f17
confusion_matrix(predict_mode(the_predictor, rows=train_set), y[train_set])

# ╔═╡ 61338c0a-852a-49e7-95b7-132ff48a7890
confusion_matrix(predict_mode(the_predictor, rows=test_set), y[test_set])

# ╔═╡ d0eabeb2-2228-45e4-9c06-49f4d334b976
md"""
### Gradient Boosting Classifier
"""

# ╔═╡ cc15f536-020b-4945-88a9-efaf102dbfd8
GradientBoostingClassifier = @load GradientBoostingClassifier pkg=MLJScikitLearnInterface

# ╔═╡ 9c1048a4-e083-481d-969b-80bf70de77e4
grad_model = GradientBoostingClassifier(n_estimators = 800)

# ╔═╡ d0339468-7988-420f-9923-802e335e8873
grad_mach = machine(grad_model,X,y, scitype_check_level=0)

# ╔═╡ 2301ca06-ff2a-4505-bf47-ac610a95f7e9
the_predictor2 = fit!(grad_mach,rows=train_set,verbosity=0)

# ╔═╡ d2dbc7c3-960e-4427-877d-419e280e1911
MLJ.predict(the_predictor2)

# ╔═╡ 598b5d46-09f7-4b84-af3b-c99755348bea
params = fitted_params(the_predictor2)

# ╔═╡ 157f00c8-ede1-430f-9ee7-6605037961b0
accuracy(predict_mode(the_predictor2, rows=train_set), y[train_set])

# ╔═╡ d7cbece1-26e5-41de-a9c2-ef5a5501474e
confusion_matrix(predict_mode(the_predictor2, rows=train_set), y[train_set])

# ╔═╡ 9700d8fa-c67c-4eef-a6b4-d370a2dc45f6
accuracy(predict_mode(the_predictor2, rows=test_set), y[test_set])

# ╔═╡ 40a881af-bfd3-4fe7-89cb-df95bc0fd6a8
confusion_matrix(predict_mode(the_predictor2, rows=test_set), y[test_set])

# ╔═╡ 5f9c65a4-f56e-4a01-a6c5-47ab7f5ff58a
md"""
### Random Forest Classifier
"""

# ╔═╡ 72d90f0d-8681-40eb-b38f-0ee7abec2f99
Forest = DecisionTree.RandomForestClassifier(n_trees=1000)

# ╔═╡ ad3933d7-be41-4d85-b73e-922d02e1d0fc
predictor3 = DecisionTree.fit!(Forest, Matrix(X),y) 

# ╔═╡ a834c923-31c4-4c3c-b65f-06d42d6a604b
y_pred = DecisionTree.predict(predictor3,Matrix(X))

# ╔═╡ afe10efd-5cdc-4bc8-b554-99dd397593f1
confusion_matrix(y,y_pred)

# ╔═╡ 19407f9c-7e1e-4a37-a175-483c4bcf131e
accuracy(confusion_matrix(y,y_pred))

# ╔═╡ Cell order:
# ╟─302a0c2a-b98a-4887-a451-3af6bde6c3e5
# ╟─bd0c048e-a055-49c7-b3f1-addf30bd90dc
# ╠═efd0d5fd-182a-4f71-958c-d8342374eaeb
# ╠═a8f52830-4534-4043-ada1-4e38f1795d10
# ╠═5bc74a25-0d17-4ede-8ddf-18670f00ce31
# ╠═400f3167-06d0-4379-875a-319e5462cd0a
# ╠═60a6278d-3108-4b2d-b537-4e72a4f9ff61
# ╟─7f2b9cae-248c-472b-9e4c-40ba980c7af9
# ╠═4ccc6a2b-8393-41a1-8cc7-53fd7c962cd0
# ╟─767c3690-f08a-49ba-b41a-dd3f2fe5fb15
# ╠═124f1b71-3d1f-4ad8-afde-28829bd9f75d
# ╠═b4438d2c-0525-40e2-b6f2-dc88bf09853e
# ╠═7250a2b6-c5a7-459f-aba7-6e914d66200c
# ╠═8b94ea34-0bba-4744-8cd3-fa71c78102ce
# ╠═fe1e1728-8b9f-439c-969a-320a1d79a0b6
# ╠═f7a2214b-98c3-4bd3-b0a7-6c0de2e62fbb
# ╠═00298d9a-87cf-4369-a27c-c9afa743f3e9
# ╠═ac2f41c0-04a1-42a2-8f73-020260a1cf0c
# ╠═f8db4414-b048-4851-9eb2-26b42d51f58c
# ╠═aa5e607c-b529-4362-ad59-ae561e683fd2
# ╠═d386f6b5-1b51-4d66-8078-cbf7d6ce1706
# ╠═a3ed4fab-568e-4999-a340-c6549b8253d2
# ╠═f4a6022f-01d7-4925-ac08-6998aa8647a4
# ╠═3a770051-bb92-4260-93ac-98ea2b6a7082
# ╟─716ed545-eb52-4d22-b4aa-0ba573161c16
# ╠═cde0eb5a-05c3-43a6-b05d-65fcb7de9225
# ╠═d166545b-5723-4f2e-84a8-ba481109ffa6
# ╠═52270e95-0922-4f1c-b450-b13484e49ef5
# ╠═82601ef7-855c-403e-b206-7fb01ecfadab
# ╟─e33fd088-6dc9-4f86-b384-7d5f0b86a974
# ╠═f8e38125-7708-4908-a0a1-c609594569d1
# ╟─e6608a84-e856-42a1-8f78-97b9e6aeb78e
# ╠═7e317040-dfdf-4d5a-8e9f-bd80423eca4c
# ╠═ebd5380c-ab2d-469d-93d8-5779dbe0f21c
# ╟─964aa138-0fa5-4289-80ee-b9aa9c0657cd
# ╠═bed92cf6-f22e-4997-bbc1-8063b369c681
# ╟─3e5af368-9cf6-4270-86e1-8e77b8961f4c
# ╠═504a1778-a24d-4272-bc72-dbb9a3fff103
# ╟─536a252d-7397-496c-a457-b1da5c531c30
# ╠═cd1c5f07-4488-4efd-bcd6-06f70150b74b
# ╠═b407062c-caa3-42b8-a53e-91e331bec8af
# ╠═cddd970f-ffe9-40c3-9129-fbfbf3f4974d
# ╠═791c9a2d-f18e-4028-97dc-bc1339898793
# ╠═b2e899dd-66c3-4d28-bd02-5bb2b238fa20
# ╠═f28336f7-548e-4760-8af8-49fa34a0d752
# ╠═da0c597c-5efe-463f-8e17-1a6a4032f7a3
# ╠═ae9fb2fe-37ee-47ef-923f-bec72e51e930
# ╠═a5dd4a8d-849a-458d-bff4-c7aa3f2b5f17
# ╠═61338c0a-852a-49e7-95b7-132ff48a7890
# ╟─d0eabeb2-2228-45e4-9c06-49f4d334b976
# ╠═cc15f536-020b-4945-88a9-efaf102dbfd8
# ╠═9c1048a4-e083-481d-969b-80bf70de77e4
# ╠═d0339468-7988-420f-9923-802e335e8873
# ╠═2301ca06-ff2a-4505-bf47-ac610a95f7e9
# ╠═d2dbc7c3-960e-4427-877d-419e280e1911
# ╠═598b5d46-09f7-4b84-af3b-c99755348bea
# ╠═157f00c8-ede1-430f-9ee7-6605037961b0
# ╠═d7cbece1-26e5-41de-a9c2-ef5a5501474e
# ╠═9700d8fa-c67c-4eef-a6b4-d370a2dc45f6
# ╠═40a881af-bfd3-4fe7-89cb-df95bc0fd6a8
# ╟─5f9c65a4-f56e-4a01-a6c5-47ab7f5ff58a
# ╠═72d90f0d-8681-40eb-b38f-0ee7abec2f99
# ╠═ad3933d7-be41-4d85-b73e-922d02e1d0fc
# ╠═a834c923-31c4-4c3c-b65f-06d42d6a604b
# ╠═afe10efd-5cdc-4bc8-b554-99dd397593f1
# ╠═19407f9c-7e1e-4a37-a175-483c4bcf131e
